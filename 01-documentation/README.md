# Veb aplikacija za naručivanje majica sa štampom

## Projektni zahtev

Administratori aplikacije se prijavljuju sa pristupnim parametrima i mogu da uređuju bazu podataka majica koje su dostupne za štampu. Administrator može da pregleda spisak majica koje su aktivne (u ponudi) ili sakrivene. Za svaku majicu mogu da navedu naziv, tip (sa kragnom, V-izrez, T-izrez, O-izrez itd), cenu majice po komadu sa štampom, boje u kojima je majica dostupna (boje su unapred definisane) i koje veličine su dostupne (XXS, XS, S, M, L, XL, XXL ili XXXL). Za svaku boju majice u kojoj je ista dostupna, administrator mora da doda posebnu fotografiju majice. Aplikacija treba da omogući korisnicima da se registruju i prijave sa svojim pristupnim parametrima. Prilikom registracije naloga, korisnici moraju da unesu kontakt podatke. Prijavljeni korisnici mogu da prelistaju bazu majica koje su u ponudi i da odaberu majicu za koju žele da izvrše pripremu motiva za štampu. Sliku motiva dodaju u PNG formatu sa svog računara ili davanjem URL putanje na Internetu, gde se datoteka slike nalazi. U interfejsu aplikacije treba nad prikazom odabrane majice (odabrane boje, takođe) da pozicioniraju motiv korišćenjem miša ili prstom (na mobilnom telefonu). Sliku motiva mogu da rotiraju ili umanje/povećaju proizvoljno, takođe korišćenjem mehanizama za upravljanje mišem ili prstom na telefonu, a kada motiv postave onako kako žele da izgleda na majici, mogu da izvrše poručivanje štampe majice. U detaljima porudžbine treba da unesu broj primeraka majice koje treba štampati, ulicu, broj i mesto za dostavu. Kada korisnik pošalje porudžbinu, na mejl radnje treba da bude poslata ZIP arhiva čije ime je ID broj porudžbine. Unutar ZIP arhive mora da se nalazi slika motiva u punoj rezoluciji u kojoj ju je korisnik otpremio, prikaz odabrane majice sa motivom postavljenim onako kako je korisnik u aplikaciji definisao prilikom postavljanja, kao i tekstualna datoteka Nalog.txt koja sadrži podatke o šifri, oznaci veličine i boji majice. Grafički interfejs sajta treba da bude realizovan sa responsive dizajnom.

## Tehnička ograničenja

...

## Razrada projektne dokumentacije
