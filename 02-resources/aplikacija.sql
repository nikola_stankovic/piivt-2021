/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP DATABASE IF EXISTS `aplikacija`;
CREATE DATABASE IF NOT EXISTS `aplikacija` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `aplikacija`;

DROP TABLE IF EXISTS `administrator`;
CREATE TABLE IF NOT EXISTS `administrator` (
  `administrator_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`administrator_id`),
  UNIQUE KEY `uq_administrator_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELETE FROM `administrator`;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;

DROP TABLE IF EXISTS `color`;
CREATE TABLE IF NOT EXISTS `color` (
  `color_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color_code` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`color_id`),
  UNIQUE KEY `uq_color_color_code` (`color_code`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELETE FROM `color`;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` (`color_id`, `color_code`) VALUES
	(8, 'black'),
	(1, 'blue'),
	(3, 'green'),
	(5, 'grey'),
	(7, 'pink'),
	(6, 'purple'),
	(2, 'red'),
	(9, 'white'),
	(4, 'yellow');
/*!40000 ALTER TABLE `color` ENABLE KEYS */;

DROP TABLE IF EXISTS `motive_image`;
CREATE TABLE IF NOT EXISTS `motive_image` (
  `motive_image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `motive_image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`motive_image_id`),
  UNIQUE KEY `uq_motive_image_motive_image_path` (`motive_image_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELETE FROM `motive_image`;
/*!40000 ALTER TABLE `motive_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `motive_image` ENABLE KEYS */;

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('pending','rejected','accepted','completed') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `number_of_copies` int(10) unsigned NOT NULL DEFAULT 1,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `house_number` int(10) unsigned NOT NULL,
  `t_shirt_color_id` int(10) unsigned NOT NULL,
  `motive_image_id` int(10) unsigned NOT NULL,
  `print_image_id` int(10) unsigned NOT NULL,
  `t_shirt_size_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `fk_order_t_shirt_color_id` (`t_shirt_color_id`),
  KEY `fk_order_motive_image_id` (`motive_image_id`),
  KEY `fk_order_print_image_id` (`print_image_id`),
  KEY `fk_order_t_shirt_size_id` (`t_shirt_size_id`),
  CONSTRAINT `fk_order_motive_image_id` FOREIGN KEY (`motive_image_id`) REFERENCES `motive_image` (`motive_image_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_order_print_image_id` FOREIGN KEY (`print_image_id`) REFERENCES `print_image` (`print_image_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_order_t_shirt_color_id` FOREIGN KEY (`t_shirt_color_id`) REFERENCES `t_shirt_color` (`t_shirt_color_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_order_t_shirt_size_id` FOREIGN KEY (`t_shirt_size_id`) REFERENCES `t_shirt_size` (`t_shirt_size_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELETE FROM `order`;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

DROP TABLE IF EXISTS `photo`;
CREATE TABLE IF NOT EXISTS `photo` (
  `photo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photo_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`photo_id`),
  UNIQUE KEY `uq_photo_photo_path` (`photo_path`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELETE FROM `photo`;
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;
INSERT INTO `photo` (`photo_id`, `photo_path`) VALUES
	(14, 'static/uploads/photo/2021/09/ocutred.jpg'),
	(15, 'static/uploads/photo/2021/09/sleevelessgreen.jpg'),
	(16, 'static/uploads/photo/2021/09/vcutblue.jpg'),
	(17, 'static/uploads/photo/2021/09/vcutred.jpg');
/*!40000 ALTER TABLE `photo` ENABLE KEYS */;

DROP TABLE IF EXISTS `print_image`;
CREATE TABLE IF NOT EXISTS `print_image` (
  `print_image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `print_image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`print_image_id`),
  UNIQUE KEY `uq_print_image_print_image_path` (`print_image_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELETE FROM `print_image`;
/*!40000 ALTER TABLE `print_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `print_image` ENABLE KEYS */;

DROP TABLE IF EXISTS `size`;
CREATE TABLE IF NOT EXISTS `size` (
  `size_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` enum('XXS','XS','S','M','L','XL','XXL','XXXL') COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`size_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELETE FROM `size`;
/*!40000 ALTER TABLE `size` DISABLE KEYS */;
INSERT INTO `size` (`size_id`, `value`) VALUES
	(1, 'XXS'),
	(2, 'XS'),
	(3, 'S'),
	(4, 'M'),
	(5, 'L'),
	(6, 'XL'),
	(7, 'XXL'),
	(8, 'XXXL');
/*!40000 ALTER TABLE `size` ENABLE KEYS */;

DROP TABLE IF EXISTS `t_shirt`;
CREATE TABLE IF NOT EXISTS `t_shirt` (
  `t_shirt_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `t_shirt_code` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('v-cut','o-cut','polo','sleeveless') COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,2) unsigned NOT NULL,
  PRIMARY KEY (`t_shirt_id`),
  UNIQUE KEY `uq_t_shirt_t_shirt_code` (`t_shirt_code`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELETE FROM `t_shirt`;
/*!40000 ALTER TABLE `t_shirt` DISABLE KEYS */;
INSERT INTO `t_shirt` (`t_shirt_id`, `t_shirt_code`, `is_active`, `name`, `type`, `price`) VALUES
	(1, 'vcutblue', 1, 'V-cut blue T-Shirt', 'v-cut', 10.00),
	(2, 'vcutred', 1, 'V-cut red T-Shirt', 'v-cut', 11.00),
	(4, 'sleevelessgreen', 0, 'Sleeveless green T-Shirt', 'sleeveless', 7.00),
	(6, 'ocutred', 1, 'O-cut red T-Shirt', 'o-cut', 13.00);
/*!40000 ALTER TABLE `t_shirt` ENABLE KEYS */;

DROP TABLE IF EXISTS `t_shirt_color`;
CREATE TABLE IF NOT EXISTS `t_shirt_color` (
  `t_shirt_color_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `t_shirt_id` int(10) unsigned NOT NULL,
  `color_id` int(10) unsigned NOT NULL,
  `photo_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`t_shirt_color_id`),
  UNIQUE KEY `uq_t_shirt_color_t_shirt_id` (`t_shirt_id`),
  KEY `fk_t_shirt_color_t_shirt_id` (`t_shirt_id`),
  KEY `fk_t_shirt_color_color_id` (`color_id`),
  KEY `fk_t_shirt_color_photo_id` (`photo_id`),
  CONSTRAINT `fk_t_shirt_color_color_id` FOREIGN KEY (`color_id`) REFERENCES `color` (`color_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_t_shirt_color_photo_id` FOREIGN KEY (`photo_id`) REFERENCES `photo` (`photo_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_t_shirt_color_t_shirt_id` FOREIGN KEY (`t_shirt_id`) REFERENCES `t_shirt` (`t_shirt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELETE FROM `t_shirt_color`;
/*!40000 ALTER TABLE `t_shirt_color` DISABLE KEYS */;
INSERT INTO `t_shirt_color` (`t_shirt_color_id`, `t_shirt_id`, `color_id`, `photo_id`) VALUES
	(27, 1, 1, 16),
	(28, 2, 2, 17),
	(29, 4, 3, 15),
	(30, 6, 2, 14);
/*!40000 ALTER TABLE `t_shirt_color` ENABLE KEYS */;

DROP TABLE IF EXISTS `t_shirt_size`;
CREATE TABLE IF NOT EXISTS `t_shirt_size` (
  `t_shirt_size_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `t_shirt_id` int(10) unsigned NOT NULL,
  `size_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`t_shirt_size_id`),
  KEY `fk_t_shirt_size_t_shirt_id` (`t_shirt_id`),
  KEY `fk_t_shirt_size_size_id` (`size_id`),
  CONSTRAINT `fk_t_shirt_size_size_id` FOREIGN KEY (`size_id`) REFERENCES `size` (`size_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_t_shirt_size_t_shirt_id` FOREIGN KEY (`t_shirt_id`) REFERENCES `t_shirt` (`t_shirt_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELETE FROM `t_shirt_size`;
/*!40000 ALTER TABLE `t_shirt_size` DISABLE KEYS */;
INSERT INTO `t_shirt_size` (`t_shirt_size_id`, `t_shirt_id`, `size_id`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 1, 3),
	(4, 1, 4),
	(5, 2, 4),
	(6, 2, 5),
	(8, 4, 3),
	(9, 4, 4),
	(10, 4, 5),
	(14, 6, 1);
/*!40000 ALTER TABLE `t_shirt_size` ENABLE KEYS */;

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `email` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_reset_code` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forename` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `uq_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `created_at`, `email`, `password_hash`, `password_reset_code`, `forename`, `surname`, `phone_number`, `postal_address`, `is_active`) VALUES
	(5, '2021-09-11 22:21:13', 'nikola.stankovic.181@singimail.rs', '$2b$11$r0AQzmwvaVe.xaGMtMGNxOLSfGwcS6ZOnmefriDfMZ4CEdRXK/b1e', NULL, 'Nikola', 'Stankovic', '+381113221847', 'Neka ulica', 1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
