import ColorService from "../components/color/service";
import PhotoService from "../components/photo/service";
import MotiveImageService from "../components/motiveimage/service";
import TshirtService from "../components/tshirt/service";
import AdministratorService from "../components/administrator/service";
import UserService from "../components/user/service";
import PrintImageService from "../components/printimage/service";
export default interface IServices {
    colorService: ColorService;
    tshirtService: TshirtService;
    photoService: PhotoService;
    administratorService: AdministratorService;
    userService: UserService;
    motiveImageService: MotiveImageService;
    printImageService: PrintImageService;
}
