import { Request, Response, NextFunction } from "express";
import ColorModel from "./model";
import IErrorResponse from "../../common/IErrorResponse.interface";
import { IAddColor, IAddColorValidator } from "./dto/AddColor";
import { IEditColor, IEditColorValidator } from "./dto/EditColor";
import BaseController from "../../common/BaseController";

class ColorController extends BaseController{

    async getAll(req: Request, res: Response, next: NextFunction) {
        const colors = await this.services.colorService.getAll();

        res.send(colors);
    }

    async getById(req: Request, res: Response, next: NextFunction) {
        const id: string = req.params.id;

        const colorId: number = +id;

        if (colorId <= 0) {
            res.sendStatus(400);
            return;
        }

        const data: ColorModel | null | IErrorResponse =
            await this.services.colorService.getById(+id);

        if (data === null) {
            res.sendStatus(404);
            return;
        }

        if (data instanceof ColorModel) {
            res.send(data);
            return;
        }

        res.status(500).send(data);
    }

    async addColor(req: Request, res: Response, next: NextFunction) {
        const data = req.body;

        if (!IAddColorValidator(data)) {
            res.status(400).send(IAddColorValidator.errors);
            return;
        }

        const result = await this.services.colorService.addColor(data as IAddColor);

        res.send(result);
    }

    async editColor(req: Request, res: Response, next: NextFunction) {
        const id: string = req.params.id;

        const colorId: number = +id;

        if (colorId <= 0) {
            res.status(400).send("Invalid ID number");
            return;
        }

        const data = req.body;

        if (!IEditColorValidator(data)) {
            res.status(400).send(IEditColorValidator.errors);
            return;
        }

        const result = await this.services.colorService.editColor(
            colorId,
            data as IEditColor
        );

        if (result === null) {
            res.sendStatus(404);
            return;
        }

        res.send(result);
    }

    async deleteById(req: Request, res: Response, next: NextFunction){
        const id: string = req.params.id;

        const colorId: number = +id;

        if(colorId <= 0){
            res.status(400).send("Invalid ID number");
            return;
        }

        res.send(await this.services.colorService.deleteById(colorId));
    }
}

export default ColorController;
