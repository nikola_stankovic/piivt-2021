import Ajv from "ajv";

interface IAddColor {
    colorCode: string;
}

const ajv = new Ajv();

const IAddColorValidator = ajv.compile({
    type: "object",
    properties: {
        colorCode: {
            type: "string",
            minLength: 2,
            maxLength: 64,
        },
    },
    required: ["colorCode"],
    additionalProperties: false,
});

export { IAddColor };
export { IAddColorValidator };
