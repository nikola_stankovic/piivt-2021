import Ajv from "ajv";

interface IEditColor {
    colorCode: string;
}

const ajv = new Ajv();

const IEditColorValidator = ajv.compile({
    type: "object",
    properties: {
        colorCode: {
            type: "string",
            minLength: 2,
            maxLength: 64,
        },
    },
    required: ["colorCode"],
    additionalProperties: false,
});

export { IEditColor };
export { IEditColorValidator };
