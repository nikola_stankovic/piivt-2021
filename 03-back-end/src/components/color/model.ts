import IModel from "../../common/IModel.interface";
class ColorModel implements IModel {
    colorId: number;
    colorCode: string;
}

export default ColorModel;
