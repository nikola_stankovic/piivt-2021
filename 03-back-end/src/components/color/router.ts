import * as express from "express";
import ColorController from "./controller";
import IApplicationResources from "../../common/IApplicationResources.interface";
import IRouter from "../../common/IRouter.interface";
import AuthMiddleware from "../../middleware/auth.middleware";

export default class ColorRouter implements IRouter {
    public setupRoutes(
        application: express.Application,
        resources: IApplicationResources
    ) {
        const colorController: ColorController = new ColorController(resources);

        application.get(
            "/color",
            AuthMiddleware.getVerifier("administrator", "user"),
            colorController.getAll.bind(colorController)
        );
        application.get(
            "/color/:id",
            AuthMiddleware.getVerifier("administrator", "user"),
            colorController.getById.bind(colorController)
        );
        application.post(
            "/color",
            AuthMiddleware.getVerifier("administrator"),
            colorController.addColor.bind(colorController)
        );
        application.put(
            "/color/:id",
            AuthMiddleware.getVerifier("administrator"),
            colorController.editColor.bind(colorController)
        );
        application.delete(
            "/color/:id",
            AuthMiddleware.getVerifier("administrator"),
            colorController.deleteById.bind(colorController)
        );
    }
}
