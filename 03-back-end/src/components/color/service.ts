import ColorModel from "./model";
import IErrorResponse from "../../common/IErrorResponse.interface";
import { IAddColor } from "./dto/AddColor";
import BaseService from "../../common/BaseService";
import { IEditColor } from "./dto/EditColor";
import IModelAdapterOptions from "../../common/IModelAdapterOptions.interface";

class ColorModelAdapterOptions implements IModelAdapterOptions {}

class ColorService extends BaseService<ColorModel> {
    protected async adaptModel(
        row: any,
        options: Partial<ColorModelAdapterOptions> = {}
    ): Promise<ColorModel> {
        const item: ColorModel = new ColorModel();

        item.colorId = Number(row?.color_id);
        item.colorCode = row?.color_code;

        return item;
    }

    public async getAll(): Promise<ColorModel[] | IErrorResponse> {
        return await this.getAllFromTable<ColorModelAdapterOptions>(
            "color",
            {}
        );
    }

    public async getById(
        colorId: number
    ): Promise<ColorModel | null | IErrorResponse> {
        return await this.getByIdFromTable<ColorModelAdapterOptions>(
            "color",
            colorId,
            {}
        );
    }

    public async addColor(
        data: IAddColor
    ): Promise<ColorModel | IErrorResponse> {
        return new Promise<ColorModel | IErrorResponse>(async (resolve) => {
            const sql = "INSERT color SET color_code = ?;";
            this.db
                .execute(sql, [data.colorCode])
                .then(async (result) => {
                    const insertInfo: any = result[0];

                    const newColorId: number = Number(insertInfo?.insertId);
                    resolve(await this.getById(newColorId));
                })
                .catch((error) => {
                    resolve({
                        errorCode: error?.errno,
                        errorMessage: error?.sqlMessage,
                    });
                });
        });
    }

    public async editColor(
        colorId: number,
        data: IEditColor
    ): Promise<ColorModel | IErrorResponse | null> {
        const result = await this.getById(colorId);

        if (result === null) {
            return null;
        }

        if (!(result instanceof ColorModel)) {
            return result;
        }

        return new Promise<ColorModel | IErrorResponse>(async (resolve) => {
            const sql = `UPDATE color SET color_code = ? WHERE color_id = ?;`;
            this.db
                .execute(sql, [data.colorCode, colorId])
                .then(async (result) => {
                    resolve(await this.getById(colorId));
                })
                .catch((error) => {
                    resolve({
                        errorCode: error?.errno,
                        errorMessage: error?.sqlMessage,
                    });
                });
        });
    }

    public async deleteById(colorId: number): Promise<IErrorResponse> {
        return new Promise<IErrorResponse>((resolve) => {
            const sql = "DELETE FROM color WHERE color_id = ?;";
            this.db
                .execute(sql, [colorId])
                .then(async (result) => {
                    const deleteInfo: any = result[0];
                    const deletedRowCount: number = Number(
                        deleteInfo?.affectedRows
                    );

                    if (deletedRowCount === 1) {
                        resolve({
                            errorCode: 0,
                            errorMessage: "One record deleted.",
                        });
                    } else {
                        resolve({
                            errorCode: -1,
                            errorMessage:
                                "This record could not be deleted because it does not exist.",
                        });
                    }
                })
                .catch((error) => {
                    if (error?.errno === 1451) {
                        resolve({
                            errorCode: -2,
                            errorMessage:
                                "This record could not be deleted beucase it's beeing used'.",
                        });
                    }
                });
        });
    }
}

export default ColorService;
