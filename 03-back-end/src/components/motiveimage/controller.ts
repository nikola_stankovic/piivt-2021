import BaseController from "../../common/BaseController";
import { Request, Response, NextFunction } from "express";
import MotiveImageModel from "./model";
import IErrorResponse from "../../common/IErrorResponse.interface";

class MotiveImageController extends BaseController {
    async getAll(req: Request, res: Response, next: NextFunction) {
        const motiveImages = await this.services.motiveImageService.getAll();

        res.send(motiveImages);
    }

    async getById(req: Request, res: Response, next: NextFunction) {
        const id: string = req.params.id;

        const motiveImageId: number = +id;

        if (motiveImageId <= 0) {
            res.sendStatus(400);
            return;
        }

        const data: MotiveImageModel | null | IErrorResponse =
            await this.services.motiveImageService.getById(+id);

        if (data === null) {
            res.sendStatus(404);
            return;
        }

        if (data instanceof MotiveImageModel) {
            res.send(data);
            return;
        }

        res.status(500).send(data);
    }

    async deleteById(req: Request, res: Response, next: NextFunction) {
        const id: string = req.params.id;

        const motiveImageId: number = +id;

        if (motiveImageId <= 0) {
            res.status(400).send("Invalid ID number");
            return;
        }

        res.send(await this.services.motiveImageService.deleteById(motiveImageId));
    }
}

export default MotiveImageController;
