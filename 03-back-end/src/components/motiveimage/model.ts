import IModel from "../../common/IModel.interface";

class MotiveImageModel implements IModel {
    motiveImageId: number;
    motiveImagePath: string;
}

export default MotiveImageModel;
