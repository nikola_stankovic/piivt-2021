import * as express from "express";
import IApplicationResources from "../../common/IApplicationResources.interface";
import IRouter from "../../common/IRouter.interface";
import MotiveImageController from "./controller";
import AuthMiddleware from "../../middleware/auth.middleware";

export default class MotiveImageRouter implements IRouter {
    public setupRoutes(
        application: express.Application,
        resources: IApplicationResources
    ) {
        const motiveImageController: MotiveImageController =
            new MotiveImageController(resources);

        application.get(
            "/motiveImage",
            AuthMiddleware.getVerifier("administrator", "user"),
            motiveImageController.getAll.bind(motiveImageController)
        );
        application.get(
            "/motiveImage/:id",
            AuthMiddleware.getVerifier("administrator", "user"),
            motiveImageController.getById.bind(motiveImageController)
        );
        application.delete(
            "/motiveImage/:id",
            AuthMiddleware.getVerifier("administrator"),
            motiveImageController.deleteById.bind(motiveImageController)
        );
    }
}
