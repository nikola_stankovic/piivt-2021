import IModelAdapterOptions from "../../common/IModelAdapterOptions.interface";
import IErrorResponse from "../../common/IErrorResponse.interface";
import BaseService from "../../common/BaseService";
import MotiveImage from "./model";

class MotiveImageAdapterOptions implements IModelAdapterOptions {}

class MotiveImageService extends BaseService<MotiveImage> {
    protected async adaptModel(
        row: any,
        options: Partial<MotiveImageAdapterOptions> = {}
    ): Promise<MotiveImage> {
        const item: MotiveImage = new MotiveImage();

        item.motiveImageId = Number(row?.motive_image_id);
        item.motiveImagePath = row?.motive_image_path;

        return item;
    }

    public async getAll(): Promise<MotiveImage[] | IErrorResponse> {
        return await this.getAllFromTable<MotiveImageAdapterOptions>(
            "motive_image",
            {}
        );
    }

    public async getById(
        motiveImageId: number
    ): Promise<MotiveImage | null | IErrorResponse> {
        return await this.getByIdFromTable<MotiveImageAdapterOptions>(
            "motive_image",
            motiveImageId,
            {}
        );
    }

    public async deleteById(motiveImageId: number): Promise<IErrorResponse> {
        return new Promise<IErrorResponse>((resolve) => {
            const sql = "DELETE FROM motive_image WHERE motive_image_id = ?;";
            this.db
                .execute(sql, [motiveImageId])
                .then(async (result) => {
                    const deleteInfo: any = result[0];
                    const deletedRowCount: number = Number(
                        deleteInfo?.affectedRows
                    );

                    if (deletedRowCount === 1) {
                        resolve({
                            errorCode: 0,
                            errorMessage: "One record deleted.",
                        });
                    } else {
                        resolve({
                            errorCode: -1,
                            errorMessage:
                                "This record could not be deleted because it does not exist.",
                        });
                    }
                })
                .catch((error) => {
                    if (error?.errno === 1451) {
                        resolve({
                            errorCode: -2,
                            errorMessage:
                                "This record could not be deleted beucase it's beeing used'.",
                        });
                    }
                });
        });
    }
}

export default MotiveImageService;
