import IModel from "../../common/IModel.interface";
import TshirtModel from "../tshirt/model";
import MotiveImageModel from "../motiveimage/model";

type OrderStatus = "pending" | "rejected" | "accepted" | "completed";

class OrderModel implements IModel {
    orderId: number;
    createdAt: Date;
    status: OrderStatus;
    numberOfCopies: number;
    location: string;
    street: string;
    houseNumber: number;
    tshirt: TshirtModel;
    motiveImage: MotiveImageModel;
    printImage: PrintImage;
}

export default OrderModel;
