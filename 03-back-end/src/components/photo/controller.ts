import BaseController from "../../common/BaseController";
import { Request, Response, NextFunction } from "express";
import PhotoModel from "./model";
import IErrorResponse from "../../common/IErrorResponse.interface";

class PhotoController extends BaseController {
    async getAll(req: Request, res: Response, next: NextFunction) {
        const photos = await this.services.photoService.getAll();

        res.send(photos);
    }

    async getById(req: Request, res: Response, next: NextFunction) {
        const id: string = req.params.id;

        const photoId: number = +id;

        if (photoId <= 0) {
            res.sendStatus(400);
            return;
        }

        const data: PhotoModel | null | IErrorResponse =
            await this.services.photoService.getById(+id);

        if (data === null) {
            res.sendStatus(404);
            return;
        }

        if (data instanceof PhotoModel) {
            res.send(data);
            return;
        }

        res.status(500).send(data);
    }

    async deleteById(req: Request, res: Response, next: NextFunction) {
        const id: string = req.params.id;

        const photoId: number = +id;

        if (photoId <= 0) {
            res.status(400).send("Invalid ID number");
            return;
        }

        res.send(await this.services.photoService.deleteById(photoId));
    }
}

export default PhotoController;
