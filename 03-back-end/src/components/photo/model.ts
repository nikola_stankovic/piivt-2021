import IModel from "../../common/IModel.interface";

class PhotoModel implements IModel {
    photoId: number;
    photoPath: string;
}

export default PhotoModel;
