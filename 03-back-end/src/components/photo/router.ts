import * as express from "express";
import IApplicationResources from "../../common/IApplicationResources.interface";
import IRouter from "../../common/IRouter.interface";
import PhotoController from "./controller";
import AuthMiddleware from "../../middleware/auth.middleware";

export default class PhotoRouter implements IRouter {
    public setupRoutes(
        application: express.Application,
        resources: IApplicationResources
    ) {
        const photoController: PhotoController = new PhotoController(resources);

        application.get(
            "/photo",
            AuthMiddleware.getVerifier("administrator", "user"),
            photoController.getAll.bind(photoController)
        );
        application.get(
            "/photo/:id",
            AuthMiddleware.getVerifier("administrator", "user"),
            photoController.getById.bind(photoController)
        );
        application.delete(
            "/photo/:id",
            AuthMiddleware.getVerifier("administrator"),
            photoController.deleteById.bind(photoController)
        );
    }
}
