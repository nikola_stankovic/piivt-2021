import IModelAdapterOptions from "../../common/IModelAdapterOptions.interface";
import IErrorResponse from "../../common/IErrorResponse.interface";
import BaseService from "../../common/BaseService";
import PhotoModel from "./model";

class PhotoModelAdapterOptions implements IModelAdapterOptions {}

class PhotoService extends BaseService<PhotoModel> {
    protected async adaptModel(
        row: any,
        options: Partial<PhotoModelAdapterOptions> = {}
    ): Promise<PhotoModel> {
        const item: PhotoModel = new PhotoModel();

        item.photoId = Number(row?.photo_id);
        item.photoPath = row?.photo_path;

        return item;
    }

    public async getAll(): Promise<PhotoModel[] | IErrorResponse> {
        return await this.getAllFromTable<PhotoModelAdapterOptions>(
            "photo",
            {}
        );
    }

    public async getById(
        photoId: number
    ): Promise<PhotoModel | null | IErrorResponse> {
        return await this.getByIdFromTable<PhotoModelAdapterOptions>(
            "photo",
            photoId,
            {}
        );
    }

    public async deleteById(photoId: number): Promise<IErrorResponse> {
        return new Promise<IErrorResponse>((resolve) => {
            const sql = "DELETE FROM photo WHERE photo_id = ?;";
            this.db
                .execute(sql, [photoId])
                .then(async (result) => {
                    const deleteInfo: any = result[0];
                    const deletedRowCount: number = Number(
                        deleteInfo?.affectedRows
                    );

                    if (deletedRowCount === 1) {
                        resolve({
                            errorCode: 0,
                            errorMessage: "One record deleted.",
                        });
                    } else {
                        resolve({
                            errorCode: -1,
                            errorMessage:
                                "This record could not be deleted because it does not exist.",
                        });
                    }
                })
                .catch((error) => {
                    if (error?.errno === 1451) {
                        resolve({
                            errorCode: -2,
                            errorMessage:
                                "This record could not be deleted beucase it's beeing used'.",
                        });
                    }
                });
        });
    }
}

export default PhotoService;
