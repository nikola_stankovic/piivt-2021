import BaseController from "../../common/BaseController";
import { Request, Response, NextFunction } from "express";
import PrintImageModel from "./model";
import IErrorResponse from "../../common/IErrorResponse.interface";

class PrintImageController extends BaseController {
    async getAll(req: Request, res: Response, next: NextFunction) {
        const printImages = await this.services.printImageService.getAll();

        res.send(printImages);
    }

    async getById(req: Request, res: Response, next: NextFunction) {
        const id: string = req.params.id;

        const printImageId: number = +id;

        if (printImageId <= 0) {
            res.sendStatus(400);
            return;
        }

        const data: PrintImageModel | null | IErrorResponse =
            await this.services.printImageService.getById(+id);

        if (data === null) {
            res.sendStatus(404);
            return;
        }

        if (data instanceof PrintImageModel) {
            res.send(data);
            return;
        }

        res.status(500).send(data);
    }

    async deleteById(req: Request, res: Response, next: NextFunction) {
        const id: string = req.params.id;

        const printImageId: number = +id;

        if (printImageId <= 0) {
            res.status(400).send("Invalid ID number");
            return;
        }

        res.send(
            await this.services.printImageService.deleteById(printImageId)
        );
    }
}

export default PrintImageController;
