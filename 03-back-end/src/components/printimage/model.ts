import IModel from "../../common/IModel.interface";

class PrintImageModel implements IModel {
    printImageId: number;
    printImagePath: string;
}

export default PrintImageModel;
