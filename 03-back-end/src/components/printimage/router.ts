import * as express from "express";
import IApplicationResources from "../../common/IApplicationResources.interface";
import IRouter from "../../common/IRouter.interface";
import PrintImageController from "./controller";
import AuthMiddleware from "../../middleware/auth.middleware";

export default class PrintImageRouter implements IRouter {
    public setupRoutes(
        application: express.Application,
        resources: IApplicationResources
    ) {
        const printImageController: PrintImageController =
            new PrintImageController(resources);

        application.get(
            "/printImage",
            AuthMiddleware.getVerifier("administrator", "user"),
            printImageController.getAll.bind(printImageController)
        );
        application.get(
            "/printImage/:id",
            AuthMiddleware.getVerifier("administrator", "user"),
            printImageController.getById.bind(printImageController)
        );
        application.delete(
            "/printImage/:id",
            AuthMiddleware.getVerifier("administrator"),
            printImageController.deleteById.bind(printImageController)
        );
    }
}
