import IModelAdapterOptions from "../../common/IModelAdapterOptions.interface";
import IErrorResponse from "../../common/IErrorResponse.interface";
import BaseService from "../../common/BaseService";
import PrintImage from "./model";

class PrintImageAdapterOptions implements IModelAdapterOptions {}

class PrintImageService extends BaseService<PrintImage> {
    protected async adaptModel(
        row: any,
        options: Partial<PrintImageAdapterOptions> = {}
    ): Promise<PrintImage> {
        const item: PrintImage = new PrintImage();

        item.printImageId = Number(row?.print_image_id);
        item.printImagePath = row?.print_image_path;

        return item;
    }

    public async getAll(): Promise<PrintImage[] | IErrorResponse> {
        return await this.getAllFromTable<PrintImageAdapterOptions>(
            "print_image",
            {}
        );
    }

    public async getById(
        printImageId: number
    ): Promise<PrintImage | null | IErrorResponse> {
        return await this.getByIdFromTable<PrintImageAdapterOptions>(
            "print_image",
            printImageId,
            {}
        );
    }

    public async deleteById(printImageId: number): Promise<IErrorResponse> {
        return new Promise<IErrorResponse>((resolve) => {
            const sql = "DELETE FROM print_image WHERE print_image_id = ?;";
            this.db
                .execute(sql, [printImageId])
                .then(async (result) => {
                    const deleteInfo: any = result[0];
                    const deletedRowCount: number = Number(
                        deleteInfo?.affectedRows
                    );

                    if (deletedRowCount === 1) {
                        resolve({
                            errorCode: 0,
                            errorMessage: "One record deleted.",
                        });
                    } else {
                        resolve({
                            errorCode: -1,
                            errorMessage:
                                "This record could not be deleted because it does not exist.",
                        });
                    }
                })
                .catch((error) => {
                    if (error?.errno === 1451) {
                        resolve({
                            errorCode: -2,
                            errorMessage:
                                "This record could not be deleted beucase it's beeing used'.",
                        });
                    }
                });
        });
    }
}

export default PrintImageService;
