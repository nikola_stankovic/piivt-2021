import BaseController from "../../common/BaseController";
import { Request, Response, NextFunction } from "express";
import { UploadedFile } from "express-fileupload";
import sizeOf from "image-size";
import Config from "../../config/dev";
import { v4 } from "uuid";
import {
    IAddTshirt,
    IAddTshirtValidator,
    IUploadedPhoto,
} from "./dto/IAddTshirt";
import { IEditTshirt, IEditTshirtValidator } from "./dto/IEditTshirt";

class TshirtController extends BaseController {
    async getAll(req: Request, res: Response, next: NextFunction) {
        const tshirts = await this.services.tshirtService.getAll();

        res.send(tshirts);
    }

    public async getById(req: Request, res: Response) {
        const id: number = Number(req.params?.id);

        if (id <= 0) {
            res.sendStatus(400);
            return;
        }

        const item = await this.services.tshirtService.getById(id);

        if (item === null) {
            res.sendStatus(404);
            return;
        }

        res.send(item);
    }

    private isPhotoValid(file: UploadedFile): {
        isOk: boolean;
        message?: string;
    } {
        try {
            const size = sizeOf(file.tempFilePath);

            const limits = Config.fileUpload.photos.limits;

            if (size.width < limits.minWidth) {
                return {
                    isOk: false,
                    message: `The ime must have a width of at least ${limits.minWidth}px.`,
                };
            }

            if (size.height < limits.minHeight) {
                return {
                    isOk: false,
                    message: `The ime must have a height of at least ${limits.minHeight}px.`,
                };
            }

            if (size.width > limits.maxWidth) {
                return {
                    isOk: false,
                    message: `The ime must have a width of at most ${limits.maxWidth}px.`,
                };
            }

            if (size.height > limits.maxHeight) {
                return {
                    isOk: false,
                    message: `The ime must have a height of at most ${limits.maxHeight}px.`,
                };
            }

            return {
                isOk: true,
            };
        } catch (e) {
            return {
                isOk: false,
                message: "Bad file format.",
            };
        }
    }

    private async uploadFiles(
        req: Request,
        res: Response
    ): Promise<IUploadedPhoto> {
        if (!req.files || Object.keys(req.files).length === 0) {
            res.status(400).send("You must upload one photo");
            return;
        }

        const fileKeys: string[] = Object.keys(req.files);
        const uploadedPhoto: IUploadedPhoto = null;

        const file = req.files[fileKeys[0]] as any;

        const result = this.isPhotoValid(file);

        if (result.isOk === false) {
            res.status(400).send(
                `Error with image ${fileKeys[0]}: "${result.message}".`
            );
            return;
        }

        const randomString = v4();
        const originalName = file?.name;
        const now = new Date();

        const imagePath =
            Config.fileUpload.uploadDestinationPhotoDirectory +
            (Config.fileUpload.uploadDestinationPhotoDirectory.endsWith("/")
                ? ""
                : "/") +
            now.getFullYear() +
            "/" +
            (now.getMonth() + 1 + "").padStart(2, "0") +
            "/" +
            randomString +
            "-" +
            originalName;

        await file.mv(imagePath);

        return { imagePath: imagePath };
    }

    public async addTshirt(req: Request, res: Response) {
        try {
            var uploadedPhoto: IUploadedPhoto = null;
            const data = JSON.parse(req.body?.data);

            if (!data.useExistingPhoto) {
                uploadedPhoto = await this.uploadFiles(req, res);
            }

            if (!IAddTshirtValidator(data)) {
                res.status(400).send(IAddTshirtValidator.errors);
                return;
            }

            const result = await this.services.tshirtService.add(
                data as IAddTshirt,
                uploadedPhoto
            );
            res.send(result);
        } catch (e) {
            res.status(400).send(e?.message);
        }
    }

    public async edit(req: Request, res: Response) {
        const id: number = Number(req.params?.id);

        if (id <= 0) {
            return res.sendStatus(400);
        }

        if (!IEditTshirtValidator(req.body)) {
            return res.status(400).send(IEditTshirtValidator.errors);
        }

        const result = await this.services.tshirtService.edit(
            id,
            req.body as IEditTshirt
        );

        if (result === null) {
            return res.sendStatus(404);
        }
        res.send(result);
    }

    public async delete(req: Request, res: Response) {
        const id: number = +req.params?.id;

        if (id <= 0) {
            return res.sendStatus(400);
        }

        const item = await this.services.tshirtService.getById(id);

        if (item === null) {
            res.sendStatus(404);
            return;
        }

        res.send(await this.services.tshirtService.delete(id));
    }
}

export default TshirtController;
