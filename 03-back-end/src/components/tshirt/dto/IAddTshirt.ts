import Ajv from "ajv";
import { TshirtSize, TshirtType } from "../model";

interface IAddTshirt {
    tshirtCode: string;
    name: string;
    tshirtType: TshirtType;
    price: number;
    tshirtSizes: TshirtSize[];
    colorId: number;
    useExistingPhoto: boolean;
    photoId: number;
}
interface IUploadedPhoto {
    imagePath: string;
}

const ajv = new Ajv();

const IAddTshirtValidator = ajv.compile({
    type: "object",
    properties: {
        tshirtCode: {
            type: "string",
            minLength: 4,
            maxLength: 64,
        },
        name: {
            type: "string",
            minLength: 6,
            maxLength: 64,
        },
        tshirtType: {
            type: "string",
        },
        price: {
            type: "number",
            minimum: 0.01,
            multipleOf: 0.01,
        },
        tshirtSizes: {
            type: "array",
            minItems: 1,
            uniqueItems: true,
            items: {
                type: "object",
                properties: {
                    sizeId: {
                        type: "number",
                        minimum: 1,
                    },
                    value: {
                        type: "string",
                        minLength: 1,
                        maxLength: 4,
                    },
                },
                required: ["sizeId", "value"],
                additionalProperties: false,
            },
        },
        colorId: {
            type: "integer",
            minimum: 1,
        },
        useExistingPhoto: {
            type: "boolean",
        },
        photoId: {
            type: "integer",
            minimum: 1,
        },
    },
    required: [
        "tshirtCode",
        "name",
        "tshirtType",
        "price",
        "tshirtSizes",
        "colorId",
        "useExistingPhoto",
    ],
    additionalProperties: false,
});

export { IAddTshirt };
export { IAddTshirtValidator };
export { IUploadedPhoto };
