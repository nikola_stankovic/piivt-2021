import Ajv from "ajv";
import { TshirtSize, TshirtType } from "../model";

interface IEditTshirt {
    tshirtCode: string;
    isActive: boolean;
    name: string;
    tshirtType: TshirtType;
    price: number;
    tshirtSizes: TshirtSize[];
    colorId: number;
    photoId: number;
}

const ajv = new Ajv();

const IEditTshirtValidator = ajv.compile({
    type: "object",
    properties: {
        tshirtCode: {
            type: "string",
            minLength: 4,
            maxLength: 64,
        },
        isActive: {
            type: "boolean",
        },
        name: {
            type: "string",
            minLength: 6,
            maxLength: 64,
        },
        tshirtType: {
            type: "string",
        },
        price: {
            type: "number",
            minimum: 0.01,
            multipleOf: 0.01,
        },
        tshirtSizes: {
            type: "array",
            minItems: 1,
            uniqueItems: true,
            items: {
                type: "object",
                properties: {
                    sizeId: {
                        type: "number",
                        minimum: 1,
                    },
                    value: {
                        type: "string",
                        minLength: 1,
                        maxLength: 4,
                    },
                },
                required: ["sizeId", "value"],
                additionalProperties: false,
            },
        },
        colorId: {
            type: "integer",
            minimum: 1,
        },
        photoId: {
            type: "integer",
            minimum: 1,
        },
    },
    required: [
        "tshirtCode",
        "name",
        "tshirtType",
        "price",
        "tshirtSizes",
        "colorId",
        "photoId",
    ],
    additionalProperties: false,
});

export { IEditTshirt };
export { IEditTshirtValidator };
