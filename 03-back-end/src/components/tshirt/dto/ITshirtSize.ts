import Ajv from "ajv";

const ajv = new Ajv();

interface ITshirtSize {
    size: "XXS" | "XS" | "S" | "M" | "L" | "XL" | "XXL" | "XXXL";
}

const ITshirtSizeValidator = ajv.compile({
    type: "object",
    properties: {
        size: "string",
    },
    required: ["size"],
    additionalProperties: false,
});

export { ITshirtSize };
export { ITshirtSizeValidator };
