import Ajv from "ajv";

const ajv = new Ajv();

interface ITshirtType {
    type: "v-cut" | "o-cut" | "polo" | "sleeveless";
}

const ITshirtTypeValidator = ajv.compile({
    type: "object",
    properties: {
        type: "string",
    },
    required: ["type"],
    additionalProperties: false,
});

export { ITshirtType };
export { ITshirtTypeValidator };
