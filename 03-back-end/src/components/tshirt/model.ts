import IModel from "../../common/IModel.interface";
import ColorModel from "../color/model";
import PhotoModel from "../photo/model";

type TshirtType = "v-cut" | "o-cut" | "polo" | "sleeveless";

class TshirtSize implements IModel {
    sizeId: number;
    value: string;
}
class TshirtModel implements IModel {
    tshirtId: number;
    tshirtCode: string;
    isActive: boolean;
    name: string;
    type: TshirtType;
    price: number;
    sizes: TshirtSize[] = [];
    color: ColorModel;
    photo: PhotoModel;
}

export default TshirtModel;
export type { TshirtSize };
export type { TshirtType };
