import express = require("express");
import IApplicationResources from "../../common/IApplicationResources.interface";
import IRouter from "../../common/IRouter.interface";
import TshirtController from "./controller";
import AuthMiddleware from "../../middleware/auth.middleware";

export default class TshirtRouter implements IRouter {
    public setupRoutes(
        application: express.Application,
        resources: IApplicationResources
    ) {
        const tshirtController: TshirtController = new TshirtController(
            resources
        );

        application.get(
            "/tshirt",
            AuthMiddleware.getVerifier("administrator", "user"),
            tshirtController.getAll.bind(tshirtController)
        );
        application.get(
            "/tshirt/:id",
            AuthMiddleware.getVerifier("administrator", "user"),
            tshirtController.getById.bind(tshirtController)
        );
        application.post(
            "/tshirt",
            AuthMiddleware.getVerifier("administrator"),
            tshirtController.addTshirt.bind(tshirtController)
        );
        application.put(
            "/tshirt/:id",
            AuthMiddleware.getVerifier("administrator"),
            tshirtController.edit.bind(tshirtController)
        );

        application.delete(
            "/tshirt/:id",
            AuthMiddleware.getVerifier("administrator"),
            tshirtController.delete.bind(tshirtController)
        );
    }
}
