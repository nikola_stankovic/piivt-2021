import BaseService from "../../common/BaseService";
import IModelAdapterOptionsInterface from "../../common/IModelAdapterOptions.interface";
import TshirtModel, { TshirtSize, TshirtType } from "./model";
import ColorModel from "../color/model";
import PhotoModel from "../photo/model";
import IErrorResponse from "../../common/IErrorResponse.interface";
import { IAddTshirt, IUploadedPhoto } from "./dto/IAddTshirt";
import { IEditTshirt } from "./dto/IEditTshirt";

class TshirtModelAdapterOptions implements IModelAdapterOptionsInterface {
    loadPhoto: boolean = false;
    loadColor: boolean = false;
    loadSize: boolean = false;
}

class TshirtService extends BaseService<TshirtModel> {
    protected async adaptModel(
        data: any,
        options: Partial<TshirtModelAdapterOptions>
    ): Promise<TshirtModel> {
        const item: TshirtModel = new TshirtModel();

        item.tshirtId = Number(data?.t_shirt_id);
        item.tshirtCode = data?.t_shirt_code;
        item.name = data?.name;
        item.isActive = Number(data?.is_active) === 1;
        item.price = Number(data?.price);

        if (options.loadPhoto) {
            item.photo = await this.getPhotoByTshirtId(item.tshirtId);
        }

        if (options.loadColor) {
            item.color = await this.getColorByTshirtId(item.tshirtId);
        }

        if (options.loadSize) {
            item.sizes = await this.getAvalibleSizesByTshirtId(item.tshirtId);
        }

        item.type = await this.getTypeByTshirtId(item.tshirtId);

        return item;
    }

    public async getAll(): Promise<TshirtModel[]> {
        const [rows] = await this.db.execute(`SELECT *
        FROM t_shirt t JOIN t_shirt_color tsc ON 
            t.t_shirt_id = tsc.t_shirt_id JOIN color c on
                c.color_id = tsc.color_id JOIN photo p on
                    p.photo_id = tsc.photo_id;`);

        if (!Array.isArray(rows) || rows.length === 0) {
            return;
        }

        const items: TshirtModel[] = [];

        for (const row of rows) {
            const data = row as any;

            const tshirtId = Number(data?.t_shirt_id);

            items.push({
                tshirtId: tshirtId,
                tshirtCode: data?.t_shirt_code,
                isActive: data?.is_active,
                name: data?.name,
                type: data?.type as TshirtType,
                price: Number(data?.price),
                color: (await this.services.colorService.getById(
                    tshirtId
                )) as ColorModel,
                photo: (await this.services.tshirtService.getPhotoByTshirtId(
                    tshirtId
                )) as PhotoModel,
                sizes: (await this.services.tshirtService.getAvalibleSizesByTshirtId(
                    tshirtId
                )) as TshirtSize[],
            });
        }
        return items;
    }

    public async getById(
        tshirtId: number,
        options: Partial<TshirtModelAdapterOptions> = {
            loadColor: true,
            loadPhoto: true,
            loadSize: true,
        }
    ): Promise<TshirtModel | null | IErrorResponse> {
        return await this.getByIdFromTable<TshirtModelAdapterOptions>(
            "t_shirt",
            tshirtId,
            options
        );
    }

    private async getPhotoByTshirtId(tshirtId: number): Promise<PhotoModel> {
        const sql = `SELECT p.* FROM
                    t_shirt t JOIN t_shirt_color tsc ON
                        t.t_shirt_id = tsc.t_shirt_id JOIN photo p ON
                            p.photo_id = tsc.photo_id
                    WHERE t.t_shirt_id = ?;`;
        const [rows] = await this.db.execute(sql, [tshirtId]);

        if (!Array.isArray(rows) || rows.length === 0) {
            return;
        }

        const photo = rows[0] as any;

        return {
            photoId: photo?.photo_id,
            photoPath: photo?.photo_path,
        };
    }

    private async getAvalibleSizesByTshirtId(
        tshirtId: number
    ): Promise<TshirtSize[]> {
        const sql = `SELECT s.*
                    FROM
                        t_shirt t JOIN t_shirt_size tss ON
                            t.t_shirt_id = tss.t_shirt_id JOIN size s ON
                                s.size_id = tss.size_id
                    WHERE t.t_shirt_id = ?;`;
        const [rows] = await this.db.execute(sql, [tshirtId]);

        if (!Array.isArray(rows) || rows.length === 0) {
            return [];
        }

        return rows.map((row) => {
            return {
                sizeId: +row?.size_id,
                value: row?.value,
            };
        });
    }

    private async getColorByTshirtId(tshirtId: number): Promise<ColorModel> {
        const sql = `SELECT c.* FROM
                    t_shirt t JOIN t_shirt_color tsc ON
                        t.t_shirt_id = tsc.t_shirt_id JOIN color c ON
                            c.color_id = tsc.color_id
                    WHERE t.t_shirt_id = ?;`;
        const [rows] = await this.db.execute(sql, [tshirtId]);

        if (!Array.isArray(rows) || rows.length === 0) {
            return;
        }

        const color = rows[0] as any;

        return {
            colorId: color?.color_id,
            colorCode: color?.color_code,
        };
    }

    private async getTypeByTshirtId(tshirtId: number): Promise<TshirtType> {
        const sql = "SELECT type FROM t_shirt WHERE t_shirt.t_shirt_id =? ;";
        const [rows] = await this.db.execute(sql, [tshirtId]);

        if (!Array.isArray(rows) || rows.length === 0) {
            return;
        }

        const data = rows[0] as any;

        return data?.type;
    }

    private async getColorIdByTshirtId(tshirtId: number): Promise<number> {
        const sql = `SELECT tsc.color_id
                     FROM t_shirt t JOIN t_shirt_color tsc ON
                        t.t_shirt_id = tsc.t_shirt_id
                    WHERE t.t_shirt_id = ?;`;
        const [rows] = await this.db.execute(sql, [tshirtId]);

        if (!Array.isArray(rows) || rows.length === 0) {
            return;
        }

        const data = rows[0] as any;

        return data;
    }

    private async getPhotoIdByTshirtId(tshirtId: number): Promise<number> {
        const sql = `SELECT tsc.photo_id
                     FROM t_shirt t JOIN t_shirt_color tsc ON
                        t.t_shirt_id = tsc.t_shirt_id
                    WHERE t.t_shirt_id = ?;`;
        const [rows] = await this.db.execute(sql, [tshirtId]);

        if (!Array.isArray(rows) || rows.length === 0) {
            return;
        }

        const data = rows[0] as any;

        return data;
    }

    public async add(
        data: IAddTshirt,
        uploadedPhoto: IUploadedPhoto
    ): Promise<TshirtModel | IErrorResponse> {
        return new Promise<TshirtModel | IErrorResponse>((resolve) => {
            this.db.beginTransaction().then(() => {
                this.db
                    .execute(
                        `INSERT t_shirt
                                SET
                                    t_shirt_code = ?,
                                    name = ?,
                                    type = ?,
                                    price = ?;`,
                        [
                            data.tshirtCode,
                            data.name,
                            data.tshirtType,
                            data.price,
                        ]
                    )
                    .then(async (res: any) => {
                        const newTshirtId: number = Number(res[0]?.insertId);

                        const promises = [];
                        if (uploadedPhoto === null) {
                            promises.push(
                                this.db.execute(
                                    `INSERT t_shirt_color
                                SET t_shirt_id = ?,
                                color_id = ?,
                                photo_id = ?;`,
                                    [newTshirtId, data.colorId, data.photoId]
                                )
                            );
                        } else {
                            promises.push(
                                this.db.execute(
                                    `INSERT photo SET photo_path = ?;`,
                                    [uploadedPhoto.imagePath]
                                )
                            );
                            promises.push(
                                this.db.execute(
                                    `INSERT t_shirt_color
                                SET t_shirt_id = ?,
                                color_id = ?,
                                photo_id = (SELECT max(photo_id) FROM photo);`,
                                    [newTshirtId, data.colorId]
                                )
                            );
                        }

                        for (const tshirtSize of data.tshirtSizes) {
                            promises.push(
                                this.db.execute(
                                    `INSERT t_shirt_size
                                SET t_shirt_id = ?,
                                size_id = ?;`,
                                    [newTshirtId, tshirtSize.sizeId]
                                )
                            );
                        }

                        Promise.all(promises)
                            .then(async () => {
                                await this.db.commit();
                                resolve(
                                    await this.services.tshirtService.getById(
                                        newTshirtId,
                                        {
                                            loadColor: true,
                                            loadPhoto: true,
                                            loadSize: true,
                                        }
                                    )
                                );
                            })
                            .catch(async (error) => {
                                await this.db.rollback();

                                resolve({
                                    errorCode: error?.errno,
                                    errorMessage: error?.sqlMessage,
                                });
                            });
                    })
                    .catch(async (error) => {
                        await this.db.rollback();

                        resolve({
                            errorCode: error?.errno,
                            errorMessage: error?.sqlMessage,
                        });
                    });
            });
        });
    }

    private editTshirt(tshirtId: number, data: IEditTshirt) {
        return this.db.execute(
            `UPDATE t_shirt
                SET 
                t_shirt_code = ?,
                is_active = ?,
                name = ?,
                type = ?,
                price = ?
            WHERE t_shirt_id = ?;`,
            [
                data.tshirtCode,
                data.isActive ? 1 : 0,
                data.name,
                data.tshirtType,
                data.price,
                tshirtId,
            ]
        );
    }

    private editPhoto(tShirtId: number, photoId: number) {
        return this.db.execute(
            `UPDATE t_shirt_color
                SET 
                photo_id = ?
            WHERE t_shirt_id = ?;`,
            [photoId, tShirtId]
        );
    }

    private editColor(tShirtId: number, colorId: number) {
        return this.db.execute(
            `UPDATE t_shirt_color
                SET 
                color_id = ?
            WHERE t_shirt_id = ?;`,
            [colorId, tShirtId]
        );
    }

    private insertOrUpdateTshirtSizes(tshirtId: number, ts: TshirtSize) {
        return this.db.execute(
            `INSERT
                t_shirt_size
            SET
                t_shirt_id = ?,
                size_id = ?;`,
            [tshirtId, ts.sizeId]
        );
    }

    public async edit(
        tshirtId: number,
        data: IEditTshirt
    ): Promise<TshirtModel | null | IErrorResponse> {
        return new Promise<TshirtModel | null | IErrorResponse>(
            async (resolve) => {
                const currentTshirt = await this.getById(tshirtId, {
                    loadColor: true,
                    loadPhoto: true,
                    loadSize: true,
                });

                if (currentTshirt === null) {
                    return resolve(null);
                }

                const rollbackAndResolve = async (error) => {
                    await this.db.rollback();
                    resolve({
                        errorCode: error?.errno,
                        errorMessage: error?.sqlMessage,
                    });
                };

                this.db
                    .beginTransaction()
                    .then(() => {
                        this.editTshirt(tshirtId, data).catch((error) => {
                            rollbackAndResolve({
                                errno: error?.errno,
                                sqlMessage:
                                    "Part t-shirt: " + error?.sqlMessage,
                            });
                        });
                    })
                    .then(async () => {
                        this.db.execute(
                            `DELETE from t_shirt_size
                                        WHERE t_shirt_id = ?;`,
                            [tshirtId]
                        );
                        for (const ts of data.tshirtSizes) {
                            this.insertOrUpdateTshirtSizes(tshirtId, ts).catch(
                                (error) => {
                                    rollbackAndResolve({
                                        errno: error?.errno,
                                        sqlMessage: `Part add/edit sizeID(${ts.sizeId}): ${error?.sqlMessage}`,
                                    });
                                }
                            );
                        }
                    })
                    .then(async () => {
                        const currentColor = await this.getColorIdByTshirtId(
                            tshirtId
                        );
                        const newColor = data.colorId;

                        if (currentColor !== newColor) {
                            this.editColor(tshirtId, newColor).catch(
                                (error) => {
                                    rollbackAndResolve({
                                        errno: error?.errno,
                                        sqlMessage:
                                            "Part color: " + error?.sqlMessage,
                                    });
                                }
                            );
                        }
                    })
                    .then(async () => {
                        const currentPhoto = await this.getPhotoIdByTshirtId(
                            tshirtId
                        );
                        const newPhoto = data.photoId;

                        if (currentPhoto !== newPhoto) {
                            this.editPhoto(tshirtId, newPhoto).catch(
                                (error) => {
                                    rollbackAndResolve({
                                        errno: error?.errno,
                                        sqlMessage:
                                            "Part photo: " + error?.sqlMessage,
                                    });
                                }
                            );
                        }
                    })
                    .then(async () => {
                        this.db.commit().catch((error) => {
                            rollbackAndResolve({
                                errno: error?.errno,
                                sqlMessage: `Part save changes: ${error?.sqlMessage}`,
                            });
                        });
                    })
                    .then(async () => {
                        resolve(
                            await this.getById(tshirtId, {
                                loadColor: true,
                                loadPhoto: true,
                                loadSize: true,
                            })
                        );
                    })
                    .catch(async (error) => {
                        await this.db.rollback();

                        resolve({
                            errorCode: error?.errno,
                            errorMessage: error?.sqlMessage,
                        });
                    });
            }
        );
    }

    private async deleteTshirtColor(tshirtId: number): Promise<boolean> {
        return new Promise<boolean>(async (resolve) => {
            this.db
                .execute(`DELETE FROM t_shirt_color WHERE t_shirt_id = ?;`, [
                    tshirtId,
                ])
                .then(() => resolve(true))
                .catch(() => resolve(false));
        });
    }

    private async deleteTshirtSize(tshirtId: number): Promise<boolean> {
        return new Promise<boolean>(async (resolve) => {
            this.db
                .execute(`DELETE FROM t_shirt_size WHERE t_shirt_id = ?;`, [
                    tshirtId,
                ])
                .then(() => resolve(true))
                .catch(() => resolve(false));
        });
    }

    private async deleteTshirt(tshirtId: number): Promise<boolean> {
        return new Promise<boolean>(async (resolve) => {
            this.db
                .execute(`DELETE FROM t_shirt WHERE t_shirt_id = ?;`, [
                    tshirtId,
                ])
                .then(() => resolve(true))
                .catch(() => resolve(false));
        });
    }

    public async delete(tshirtId: number): Promise<IErrorResponse | null> {
        return new Promise<IErrorResponse>(async (resolve) => {
            const currentTshirt = await this.getById(tshirtId, {
                loadColor: true,
                loadPhoto: true,
            });

            if (currentTshirt === null) {
                return resolve(null);
            }

            this.db
                .beginTransaction()
                .then(async () => {
                    if (await this.deleteTshirtColor(tshirtId)) return;
                    throw {
                        errno: -1002,
                        sqlMessage: "Could not delete from t_shirt_color",
                    };
                })
                .then(async () => {
                    if (await this.deleteTshirtSize(tshirtId)) return;
                    throw {
                        errno: -1002,
                        sqlMessage: "Could not delete from t_shirt_size",
                    };
                })
                .then(async () => {
                    if (await this.deleteTshirt(tshirtId)) return;
                    throw {
                        errno: -1002,
                        sqlMessage: "Could not delete from t_shirt",
                    };
                })
                .then(async (filesTDelete) => {
                    await this.db.commit();
                })
                .then(() => {
                    resolve({
                        errorCode: 0,
                        errorMessage: "Tshirt deleted!",
                    });
                })
                .catch(async (error) => {
                    await this.db.rollback();
                    resolve({
                        errorCode: error?.errno,
                        errorMessage: error?.sqlMessage,
                    });
                });
        });
    }
}

export default TshirtService;
