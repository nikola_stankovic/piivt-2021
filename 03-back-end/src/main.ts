import * as express from "express";
import * as cors from "cors";
import Config from "./config/dev";
import ColorRouter from "./components/color/router";
import * as mysql2 from "mysql2/promise";
import IApplicationResources from "./common/IApplicationResources.interface";
import Router from "./router";
import ColorService from "./components/color/service";
import fileUpload = require("express-fileupload");
import TshirtService from "./components/tshirt/service";
import TshirtRouter from "./components/tshirt/router";
import PhotoService from "./components/photo/service";
import PhotoRouter from "./components/photo/router";
import AdministratorService from "./components/administrator/service";
import AdministratorRouter from "./components/administrator/router";
import UserRouter from "./components/user/router";
import UserService from "./components/user/service";
import AuthRouter from "./components/auth/router";
import MotiveImageRouter from "./components/motiveimage/router";
import MotiveImageService from "./components/motiveimage/service";
import PrintImageService from "./components/printimage/service";
import PrintImageRouter from "./components/printimage/router";

async function main() {
    const application: express.Application = express();

    application.use(
        cors({
            origin: "http://localhost:3000",
            credential: true,
        })
    );
    application.use(express.json());
    application.use(
        fileUpload({
            limits: {
                fileSize: Config.fileUpload.maxSize,
                files: Config.fileUpload.maxFiles,
            },
            useTempFiles: true,
            tempFileDir: Config.fileUpload.temporaryDirectory,
            uploadTimeout: Config.fileUpload.timeout,
            safeFileNames: true,
            preserveExtension: true,
            createParentPath: true,
            abortOnLimit: true,
        })
    );

    const resources: IApplicationResources = {
        databaseConnection: await mysql2.createConnection({
            host: Config.database.host,
            port: Config.database.port,
            user: Config.database.user,
            password: Config.database.password,
            database: Config.database.database,
            charset: Config.database.charset,
            timezone: Config.database.timezone,
            supportBigNumbers: true,
        }),
    };

    resources.databaseConnection.connect();

    resources.services = {
        colorService: new ColorService(resources),
        tshirtService: new TshirtService(resources),
        photoService: new PhotoService(resources),
        administratorService: new AdministratorService(resources),
        userService: new UserService(resources),
        motiveImageService: new MotiveImageService(resources),
        printImageService: new PrintImageService(resources),
    };

    application.use(
        Config.server.static.route,
        express.static(Config.server.static.path, {
            index: Config.server.static.index,
            cacheControl: Config.server.static.cacheControl,
            maxAge: Config.server.static.maxAge,
            etag: Config.server.static.etag,
            dotfiles: Config.server.static.dotfiles,
        })
    );

    Router.setupRoutes(application, resources, [
        new ColorRouter(),
        new TshirtRouter(),
        new PhotoRouter(),
        new AdministratorRouter(),
        new UserRouter(),
        new AuthRouter(),
        new MotiveImageRouter(),
        new PrintImageRouter(),
    ]);

    application.use((req, res) => {
        res.sendStatus(404);
    });

    application.use((err, req, res, next) => {
        res.status(err.status).send(err.type);
    });

    application.listen(Config.server.port);
}

main();
