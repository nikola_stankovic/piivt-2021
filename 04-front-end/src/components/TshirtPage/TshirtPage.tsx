import { Link } from "react-router-dom";
import {
    ControlLabel,
    Form,
    FormGroup,
    FormControl,
    Radio,
    RadioGroup,
    Input,
} from "rsuite";
import BasePage, { BasePageProperties } from "../BasePage/BasePage";
import TshirtModel from "../../../../03-back-end/src/components/tshirt/model";
import TshirtService from "../../services/TShirtService";
// import Img from "../../util/photo/0fb75579-b867-4020-afab-7821622f0a64-istockphoto-533870481-170667a.jpg";
interface OrderForm {
    size: string | null;
    numberOfCopies: number | null;
}

class TshirtPageProperties extends BasePageProperties {
    match?: {
        params: {
            tsid: string;
        };
    };
}

class TshirtPageState {
    title: string = "";
    tshirts: TshirtModel[] = [];
    tshirt: TshirtModel | null;
    orderForm: OrderForm;
}

export default class TshirtPage extends BasePage<TshirtPageProperties> {
    state: TshirtPageState;

    constructor(props: TshirtPageProperties) {
        super(props);

        this.state = {
            title: "Loading...",
            tshirts: [],
            tshirt: null,
            orderForm: this.initialOrderForm(),
        };
    }

    private initialOrderForm(): OrderForm {
        return {
            size: null,
            numberOfCopies: null,
        };
    }
    private getTshirtId(): number | null {
        const tsid = this.props.match?.params.tsid;
        return tsid ? +tsid : null;
    }

    private getTshirtData() {
        const tsId = this.getTshirtId();

        if (tsId === null) {
            this.apiGetAllTshirts();
        } else {
            this.apiGetTshirt(tsId);
        }
    }

    private apiGetAllTshirts() {
        TshirtService.getAllTshirts().then((tshirts) => {
            if (tshirts.length === 0) {
                return this.setState({
                    title: "No T-shirts found",
                    tshirts: [],
                    tshirt: null,
                    orderForm: this.initialOrderForm(),
                });
            }
            this.setState({
                ...this.state,
                title: "All T-shirts",
                tshirts,
            });
        });
    }

    private apiGetTshirt(tsId: number) {
        TshirtService.getTshirtById(tsId).then((result) =>
            result === null
                ? this.setState({
                      ...this.state,
                      title: "T-shirt not found",
                      tshirts: [],
                      tshirt: null,
                  })
                : this.setState({
                      ...this.state,
                      title: result.name,
                      tshirts: [],
                      tshirt: result,
                  })
        );
    }

    componentDidMount() {
        this.getTshirtData();
    }

    componentDidUpdate(
        prevProps: TshirtPageProperties,
        prevState: TshirtPageState
    ) {
        if (prevProps.match?.params.tsid !== this.props.match?.params.tsid) {
            this.getTshirtData();
        }
    }

    renderMain(): JSX.Element {
        const { tshirt, tshirts, title, orderForm } = this.state;
        const sizes = ["XXS", "XS", "S", "M", "L", "XL", "XXL", "XXXL"];
        const logo =
            tshirt !== null
                ? require(`../../util/photo/${tshirt.photo.photoPath.substring(
                      tshirt.photo.photoPath.lastIndexOf("/") + 1
                  )}`).default
                : null;
        console.log(logo);
        return (
            <>
                <h1> {title}</h1>
                {tshirt === null ? (
                    <>
                        <p>T-shirts:</p>
                        <ul>
                            {tshirts.map((tshirt) => (
                                <li key={"tshirt-link-" + tshirt.tshirtId}>
                                    <Link to={"/tshirt/" + tshirt.tshirtId}>
                                        {tshirt.name}
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </>
                ) : (
                    <Form
                        onChange={(orderForm) =>
                            this.setState({ ...this.state, orderForm })
                        }
                    >
                        <div style={{ display: "flex" }}>
                            {
                                // eslint-disable-next-line jsx-a11y/img-redundant-alt
                                <img
                                    src={logo}
                                    alt="missing photo"
                                    width={300}
                                />
                            }
                            <div>
                                <FormGroup style={{ padding: "10px 0px" }}>
                                    <ControlLabel>Name</ControlLabel>
                                    <FormControl
                                        disabled={true}
                                        readOnly={true}
                                        value={tshirt.name}
                                    />
                                </FormGroup>
                                <FormGroup style={{ padding: "10px 0px" }}>
                                    <ControlLabel>
                                        Number of copies
                                    </ControlLabel>
                                    <FormControl
                                        accepter={Input}
                                        name="numberOfCopies"
                                        errorMessage={
                                            orderForm.numberOfCopies != null &&
                                            !isNaN(orderForm.numberOfCopies)
                                                ? ""
                                                : "Insert a number"
                                        }
                                    />
                                </FormGroup>
                                <FormGroup style={{ padding: "10px 0px" }}>
                                    <ControlLabel>Available sizes</ControlLabel>
                                    <FormControl
                                        name="radio"
                                        accepter={RadioGroup}
                                    >
                                        <div style={{ display: "flex" }}>
                                            {sizes.map((size) => (
                                                <Radio
                                                    value={size}
                                                    disabled={
                                                        tshirt.sizes.filter(
                                                            (x) =>
                                                                size === x.value
                                                        ).length !== 1
                                                    }
                                                    style={{ marginRight: 10 }}
                                                >
                                                    {size}
                                                </Radio>
                                            ))}
                                        </div>
                                    </FormControl>
                                </FormGroup>
                                <div style={{ display: "flex" }}>
                                    <FormGroup
                                        style={{
                                            padding: "10px 10px 10px 0px",
                                        }}
                                    >
                                        <ControlLabel>City</ControlLabel>
                                        <FormControl
                                            accepter={Input}
                                            name="city"
                                        />
                                    </FormGroup>
                                    <FormGroup style={{ padding: "10px 0px" }}>
                                        <ControlLabel>Address</ControlLabel>
                                        <FormControl
                                            accepter={Input}
                                            name="address"
                                        />
                                    </FormGroup>
                                </div>
                            </div>
                        </div>
                    </Form>
                )}
            </>
        );
    }
}
