import { resolve } from "dns";
import TshirtModel from "../../../03-back-end/src/components/tshirt/model";
import api from "../api/api";

export default class TshirtService {
    public static getAllTshirts(): Promise<TshirtModel[]> {
        return new Promise<TshirtModel[]>((resolve) => {
            api("get", "/tshirt", "user").then((res) => {
                if (res?.status !== "ok") {
                    return resolve([]);
                }
                resolve(res.data as TshirtModel[]);
            });
        });
    }

    public static getTshirtById(tshirtId: number): Promise<TshirtModel | null> {
        return new Promise<TshirtModel | null>((resolve) => {
            api("get", "/tshirt/" + tshirtId, "user").then((res) => {
                if (res?.status !== "ok") {
                    return resolve(null);
                }
                resolve(res.data as TshirtModel);
            });
        });
    }
}
